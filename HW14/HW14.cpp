#include <iostream>
#include <string>

int main()
{
    std::string text("Some text");

    std::cout << text << "\n";

    std::cout << "Length: " << text.length() << "\n";

    std::cout << "Starts with: " << text.at(0) << "\n";

    std::cout << "Ends with: " << text.at(text.length() - 1) << "\n";
}

